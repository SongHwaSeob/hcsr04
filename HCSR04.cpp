#include "HCSR04.hpp"

HCSR04::HCSR04()
    : triggerPin(0), echoPin(1)
{
  // Set pin modes
  pinMode(triggerPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

HCSR04::~HCSR04()
{
  // Empty
}

float HCSR04::measure()
{
  // Send trigger impulse
  digitalWrite(triggerPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(triggerPin, LOW);

  // Wait until we receive an echo
  while (digitalRead(echoPin) == 0)
    delayMicroseconds(1);

  int usCounter = 0;

  // Measure echo signal width
  while (digitalRead(echoPin) == 1)
  {
    usCounter++;
    delayMicroseconds(1);
  }

  // Calculate distance
  // us to seconds
  float seconds = static_cast<float>(usCounter) / 1000000.0f;

  // Multiply with the speed of sound in cm/s
  return seconds * 34029.0f;
}

void HCSR04::setTriggerPin(uint8_t triggerPin)
{
  this->triggerPin = triggerPin;
  pinMode(this->triggerPin, OUTPUT);
}

void HCSR04::setEchoPin(uint8_t echoPin)
{
  this->echoPin = echoPin;
  pinMode(this->echoPin, INPUT);
}
