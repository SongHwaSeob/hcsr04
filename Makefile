CC      = g++
CFLAGS  = -Wall -g
LDFLAGS = -L/usr/local/libs -lwiringPi

OBJ = main.o HCSR04.o

all: $(OBJ)
	$(CC) $(CFLAGS) -o hcsr04 $(OBJ) $(LDFLAGS)

clean:
	rm -rf hcsr04 $(OBJ)